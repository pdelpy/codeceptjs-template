# E2E tests with CodeceptJS, Webdriver and BDD Plugin  

![pipeline](https://gitlab.com/pdelpy/codeceptjs-template/badges/master/pipeline.svg)

This is a demonstration project of integration tests. In this project the user try to connect to an app.  
These tests are developed in TypeScript with [CodeceptJS](https://codecept.io/), [Webdriver](https://codecept.io/) and [BDD plugin](https://codecept.io/bdd/#advanced-gherkin)

## Features

-   [TypeScript](https://www.typescriptlang.org/docs/home.htm)
-   chai
-   Page Object Pattern
-   eslint
-   prettier
-   Allure report (screenshots on failure) and Timeline report
-   [Browserstack](https://www.browserstack.com/)

## Requirements

-   node >= 12.18.x - [how to install Node](https://nodejs.org/en/download/)
-   npm >= 6.14.x - [how to install NPM](https://www.npmjs.com/get-npm)
-   openJDK >= 11.x.x - [how to install JDK](https://openjdk.java.net/install/)

## Getting Started

Install the dependencies:

```bash
npm install
```

Run e2e tests:

```bash
npm run test
```

Run e2e tests in debug mode:

If you want to debug a code launch the following command after setup a breakpoint into your code

```bash
npm run test -- --debug
```

Run e2e tests with functional scope (ex: `@smoke`):

If you want to test with a functional scope launch the following command

```bash
npm run test -- --grep "@smoke"
```

## Spoken Languages

If you want to use another language in features files, you can see this [doc](https://cucumber.io/docs/gherkin/reference/#spoken-languages) about how can you do that.

## Reports

### Allure

Run this command to generate the allure report in the directory `.tmp/allure-report`:

```bash
npm run report:generate
```

You can run this command to generate the report and start a server on your machine and open report on the browser:

```bash
npm run report
```

## Tslint

Run to format the code:

```bash
npm run lint
```

## Browserstack

For execute tests into browserstack engine you need :

**Create account**

1.  Create a account on [browserstack](https://www.browserstack.com) (you have 90min free automation)
2.  Get the `access key` and the `username` into [automatisation section](https://automate.browserstack.com/),
3.  Set the environement variable BROWSERSTACK_USER and BROWSERSTACK_KEY with the creadentials retrieve

**Run e2e tests with browserstack**

```sh
npm run browserstack
```