/// <reference types='codeceptjs' />

declare namespace CodeceptJS {
  interface Methods extends WebDriver {}
  interface I extends WithTranslation<Methods> {}
  namespace Translation {
    interface Actions {}
  }
}
