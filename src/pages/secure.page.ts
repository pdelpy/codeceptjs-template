import { expect } from 'chai';
import Page from './page';

const I = actor();

class SecurePage extends Page {
    get flashAlert() {
        return '#flash';
    }

    async checkMessage(message: string) {
        I.seeElement(this.flashAlert);
        const txt = await I.grabTextFrom(this.flashAlert);
        expect(txt).contains(message);
    }
}

export default new SecurePage();
