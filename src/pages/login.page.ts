import Page from './page';

const I = actor();

class LoginPage extends Page {
    get inputUsername() {
        return '#username';
    }
    get inputPassword() {
        return '#password';
    }
    get btnSubmit() {
        return 'button[type="submit"]';
    }

    async login(username: string, password: string) {
        I.fillField(this.inputUsername, username);
        I.fillField(this.inputPassword, password);
        I.click(this.btnSubmit);
    }

    open() {
        return super.open('login');
    }
}

export default new LoginPage();
