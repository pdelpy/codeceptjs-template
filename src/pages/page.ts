const I = actor();

export default class Page {
    open(path: string): void {
        I.amOnPage(`https://the-internet.herokuapp.com/${path}`);
    }
}
