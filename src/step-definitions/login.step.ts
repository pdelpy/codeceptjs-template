import LoginPage from '../pages/login.page';
import SecurePage from '../pages/secure.page';

const pages: any = {
    login: LoginPage,
};

Given(/^I am on the (\w+) page$/, (page: string) => {
    pages[page].open();
});

When(/^I login with (\w+) and (.+)$/, (username: string, password: string) => {
    LoginPage.login(username, password);
});

Then(/^I should see a flash message saying (.*)$/, async (message: string) => {
    await SecurePage.checkMessage(message);
});
