const wedriverConfig = require('./codecept.webdriver.conf').config;

exports.config = {
  ...wedriverConfig,
  helpers: {
    WebDriver: {
      url: 'http://localhost',
      restart: false,
      host: 'hub.browserstack.com',
      path: '/wd/hub',
      user: process.env.BROWSERSTACK_USER, // credentials
      key: process.env.BROWSERSTACK_KEY, // credentials
      browser: 'chrome',
      desiredCapabilities: {
        'project': 'CodeceptJS',
        'browserstack.debug' : 'true',
        'browserstack.networkLogs' : 'true'
      }
    },
  },
  plugins: {
    wdio: {
      enabled: true
    }
  }
}