const { setHeadlessWhen } = require('@codeceptjs/configure');
const { execSync } = require('child_process');

const getPaths = (type = "step") => {
  let stdout = execSync(`find ${__dirname}/.. -type f -name *.${type}.ts`, {encoding: 'utf8'});
  let files = stdout.split("\n");
  files.pop();
  return files;
}

setHeadlessWhen(process.env.HEADLESS);

const config = {
  output: '../.tmp',
  helpers: {
    WebDriver: {
      url: 'http://localhost',
      restart: false,
      browser: 'chrome'
    },
  },

  gherkin: {
    features: '../src/features/*.feature',
    steps: getPaths()
  },

  include: [],

  plugins: {
    allure: {
      outputDir: '.tmp/allure-results',
      disableWebdriverStepsReporting: true,
      disableWebdriverScreenshotsReporting: false,
      useCucumberStepReporter: true,
    },
    wdio: {
      enabled: true,
      services: ['selenium-standalone']
    }
  },  
  bootstrap: null,
  mocha: {},
  require:['ts-node/register'],
  name: 'codecept demo tests'
}

exports.config = config